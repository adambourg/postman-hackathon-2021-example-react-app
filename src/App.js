import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Hello world!</h1>

      <p> This is a basic app for the postman hack a thon project 2021. We're not doing really just using it as a gitlab example. </p>
    </div>
  );
}

export default App;
